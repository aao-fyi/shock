---
title: 502
description: Bad Gateway
icon: construction
type: error
url: 502.html
sitemap_exclude: true
breadcrumb: false
heading: false
sidebar: false
draft: false
---

Invalid response from upstream server.
