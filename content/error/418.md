---
title: 418
description: I'm a Teapot
icon: coffee
type: error
url: 418.html
sitemap_exclude: true
breadcrumb: false
heading: false
sidebar: false
draft: false
---

The server refuses to brew coffee because it is, permanently, a teapot.
