---
title: 500
description: Internal Server Error
icon: bug
type: error
url: 500.html
sitemap_exclude: true
breadcrumb: false
heading: false
sidebar: false
draft: false
---

Try loading a different page or checking back in a few minutes.
