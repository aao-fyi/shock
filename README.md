# Shock
Shock is a Hugo theme for static sites.

Refer to the [project site](https://shock.aao.fyi/) and [documentation](https://shock.aao.fyi/docs/) for more information.

## Issues
Open new issues in the [Codeberg Issue Tracker](https://codeberg.org/aao-fyi/shock/issues).

## License
Shock is distributed under the [MIT License](https://codeberg.org/aao-fyi/shock/src/branch/main/LICENSE).
