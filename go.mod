module codeberg.org/aao-fyi/shock

go 1.20

require (
	github.com/lucide-icons/lucide v0.0.0-20241001201341-542507f8358c // indirect
	github.com/simple-icons/simple-icons v0.0.0-20240929022857-8be6321b1e5f // indirect
	github.com/twbs/bootstrap v5.3.3+incompatible // indirect
	github.com/twbs/rfs v10.0.0+incompatible // indirect
)
