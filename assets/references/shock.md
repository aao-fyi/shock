<!-- Shock -->
[^bootstrap-git]: The Bootstrap Team. "[Bootstrap GitHub](https://github.com/twbs/bootstrap)." 2025.
[^chroma-git]: Alec Thomas. "[Chroma GitHub](https://github.com/alecthomas/chroma)." 2025.
[^hugo-git]: The Hugo Authors. "[Hugo GitHub](https://github.com/gohugoio/hugo)." 2025.
[^lucide-git]: Lucide Contributors & Cole Bemis. "[Lucide GitHub](https://github.com/lucide-icons/lucide)." 2025.
[^postcss-git]: Andrey Sitnik. "[PostCSS GitHub](https://github.com/postcss/postcss)." 2025.
[^purgecss-git]: Full Human. "[PurgeCSS GitHub](https://github.com/FullHuman/purgecss)." 2025.
[^simple-icons-git]: Simple Icons Contributors. "[Simple Icons GitHub](https://github.com/simple-icons/simple-icons)." 2025.
